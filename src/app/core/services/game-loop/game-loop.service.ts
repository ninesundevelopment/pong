import { Injectable, NgZone } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface TickTime {
  current: number;
  delta: number;
}

@Injectable({
  providedIn: 'root',
})
export class GameLoopService {

  private simulationTimestep       = 1000 / 30;
  private frameDelta               = 0;
  private lastFrameTimeMs          = 0;
  private lastFrameDelta           = 0;
  private fps                      = 60;
  private fpsAlpha                 = 0.9;
  private fpsUpdateInterval        = 1000;
  private lastFpsUpdate            = 0;
  private framesSinceLastFpsUpdate = 0;
  private numUpdateSteps           = 0;
  private minFrameDelay            = 0;
  private running                  = false;
  private started                  = false;
  private panic                    = false;
  private animationFrame           = null;


  public currentTime: number = 0;
  public deltaTime: number   = 0;
  public maxFPS: number      = 60;

  public update: BehaviorSubject<TickTime> = new BehaviorSubject({current: 0, delta: 0});
  public draw: BehaviorSubject<TickTime>   = new BehaviorSubject({current: 0, delta: 0});

  constructor(
    private zone: NgZone,
  ) {
  }

  start() {
    this.zone.runOutsideAngular(() => {
      if (!this.started) {
        this.started = true;

        this.animationFrame = requestAnimationFrame((timestamp) => {
          this.draw.next({current: timestamp, delta: 0});

          this.running = true;

          this.lastFrameTimeMs          = timestamp;
          this.lastFpsUpdate            = timestamp;
          this.framesSinceLastFpsUpdate = 0;

          // Start the main loop.
          this.animationFrame = requestAnimationFrame(this.animate.bind(this));
        });
      }
      return this;
    });
  }

  stop() {
    this.running = false;
    this.started = false;
    cancelAnimationFrame(this.animationFrame);
    return this;
  }

  getSimulationTimestep() {
    return this.simulationTimestep;
  }

  setSimulationTimestep(timestep: number) {
    this.simulationTimestep = timestep;
    return this;
  };

  getFPS() {
    return this.fps;
  }

  getMaxAllowedFPS() {
    return 1000 / this.minFrameDelay
  }

  setMaxAllowedFPS() {
    if (typeof this.fps === 'undefined') {
      this.fps = Infinity;
    }
    if (this.fps === 0) {
      this.stop();
    } else {
      // Dividing by Infinity returns zero.
      this.minFrameDelay = 1000 / this.fps;
    }
    return this;
  }

  resetFrameDelta() {
    const oldFrameDelta = this.frameDelta;
    this.frameDelta     = 0;
    return oldFrameDelta;
  }

  isRunning() {
    return this.running;
  }

  animate(timestamp) {
    this.zone.runOutsideAngular(() => {
    this.animationFrame = requestAnimationFrame(this.animate.bind(this));

    if (timestamp < this.lastFrameTimeMs + this.minFrameDelay) {
      return;
    }

    this.frameDelta += timestamp - this.lastFrameTimeMs;
    this.lastFrameTimeMs = timestamp;

    if (timestamp > this.lastFpsUpdate + this.fpsUpdateInterval) {

      this.fps =
        this.fpsAlpha * this.framesSinceLastFpsUpdate * 1000 / (timestamp - this.lastFpsUpdate) +
        (1 - this.fpsAlpha) * this.fps;

      this.lastFpsUpdate            = timestamp;
      this.framesSinceLastFpsUpdate = 0;
    }

    this.framesSinceLastFpsUpdate++;

    this.numUpdateSteps = 0;
    while (this.frameDelta >= this.simulationTimestep) {
      this.update.next({current: timestamp, delta: this.frameDelta});
      this.frameDelta -= this.simulationTimestep;

      if (++this.numUpdateSteps >= 240) {
        this.panic = true;
        break;
      }
    }

    this.draw.next({current: timestamp, delta: this.frameDelta});

    this.panic = false;
    });
  }
}
