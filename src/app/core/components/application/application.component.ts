import { Component, OnInit } from '@angular/core';
import { GameLoopService } from '@app/core/services/game-loop/game-loop.service';


@Component({
  selector: 'application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss'],
})
export class ApplicationComponent implements OnInit {

  constructor(
    public gameLoopService: GameLoopService,
  ) {}

  ngOnInit() {
  }
}
