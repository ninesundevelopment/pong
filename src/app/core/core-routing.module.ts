import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'game',
    loadChildren: '../views/game/game.module#GameModule',
  },
  {
    path: '',
    redirectTo: 'game',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    enableTracing: false,
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
    urlUpdateStrategy: 'eager',
  })],
  exports: [RouterModule],
})
export class CoreRoutingModule {
}
