import { MatDialogConfig } from '@angular/material';

export const DefaultDialogConfig: MatDialogConfig = {
  width: '90%',
  height: '90%',
  disableClose: true,
};
