import { Component, ContentChild, Input, OnInit, TemplateRef } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { fromEvent, timer } from 'rxjs';
import { debounce } from 'rxjs/operators';

@Component({
  selector: 'dialog-wrapper',
  templateUrl: './dialog-wrapper.component.html',
  styleUrls: ['./dialog-wrapper.component.scss'],
})
export class DialogWrapperComponent implements OnInit {

  @Input() dialogRef: MatDialogRef<any>;
  @Input() title: string;

  public maxHeight = 0;

  @ContentChild('header') header: TemplateRef<any>;
  @ContentChild('body') body: TemplateRef<any>;
  @ContentChild('footer') footer: TemplateRef<any>;

  constructor() {
  }

  ngOnInit() {
    this.maxHeight = window.innerHeight * 0.9;

    fromEvent(window, 'resize').pipe(debounce(() => timer(100))).subscribe(this.onResize.bind(this));
  }

  onResize() {
    this.maxHeight = window.innerHeight * 0.9 - 48;
  }
}
