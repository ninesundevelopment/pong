import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogHeaderComponent } from './components/dialog-header/dialog-header.component';
import { DialogBodyComponent } from './components/dialog-body/dialog-body.component';
import { DialogFooterComponent } from './components/dialog-footer/dialog-footer.component';
import { CloseComponent } from '@app/dialogs/components/close/close.component';
import { DialogWrapperComponent } from './components/dialog-wrapper/dialog-wrapper.component';
import { SharedModule } from '@app/shared/shared.module';

@NgModule({
  declarations: [
    CloseComponent,
    DialogHeaderComponent,
    DialogBodyComponent,
    DialogFooterComponent,
    DialogWrapperComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  entryComponents: [
  ],
})
export class DialogsModule {
}
