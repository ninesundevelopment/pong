import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatAutocompleteModule,
  MatCheckboxModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatTableModule,
  MatTabsModule,
} from '@angular/material';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    CommonModule,
    MatDialogModule,
    MatInputModule,
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatFormFieldModule,
  ],
  exports: [
    MatDialogModule,
    MatInputModule,
    MatTabsModule,
    MatTableModule,
    MatCheckboxModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatFormFieldModule,
  ],
})
export class SharedModule {
}
