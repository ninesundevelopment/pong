export class Vector2 {

  public x: number;
  public y: number;

  constructor(x: number = 0, y: number = 0) {
    this.x = x;
    this.y = y;
  }

  public static Down: () => Vector2   = () => new Vector2(0, 1);
  public static Left: () => Vector2   = () => new Vector2(-1, 0);
  public static One: () => Vector2    = () => new Vector2(1, 1);
  public static Right: () => Vector2  = () => new Vector2(1, 0);
  public static Up: () => Vector2     = () => new Vector2(0, -1);
  public static Zero: () => Vector2   = () => new Vector2(0, 0);
  public static Random: () => Vector2 = () => Vector2.Up().rotate(Math.random() * 361);

  public magnitude: () => number      = () => Math.sqrt(this.x * this.x + this.y * this.y);
  public equals: (Vector2) => boolean = (b: Vector2) => this.x === b.x && this.y === b.y;

  public add(b: Vector2): Vector2 {
    const x = this.x + b.x;
    const y = this.y + b.y;

    return new Vector2(x, y);
  }

  public times(b: number): Vector2 {
    const x = this.x * b;
    const y = this.y * b;

    return new Vector2(x, y);
  }

  public toString() {
    return JSON.stringify({x: this.x, y: this.y});
  }

  public rotate(ang: number) {
    ang = -ang * (Math.PI / 180);

    const cos = Math.cos(ang);
    const sin = Math.sin(ang);

    return new Vector2(
      Math.round(10000 * (this.x * cos - this.y * sin)) / 10000,
      Math.round(10000 * (this.x * sin + this.y * cos)) / 10000,
    );
  };

  public angle() {
    return Math.ceil(Math.atan(this.y / this.x) * -180 / Math.PI);
  }

  public unit(): Vector2 {
    const mag = this.magnitude();
    return new Vector2(this.x / mag, this.y / mag);
  }

}
