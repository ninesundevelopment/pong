import { Vector2 } from '@app/shared/classes/vector2';
import { Inject } from '@angular/core';
import { GameLoopService, TickTime } from '@app/core/services/game-loop/game-loop.service';


export class Entity {
  private gameLoop: GameLoopService;
  private _element: HTMLElement;
  private _position: Vector2;
  private _speed: Vector2;

  private _dimension: Vector2;

  constructor(
    element: HTMLElement,
    position: Vector2 = new Vector2(),
    speed: Vector2    = new Vector2(),
  ) {
    this._element   = element;
    this._position  = position;
    this._speed     = speed;
    this._dimension = new Vector2(
      element.clientWidth,
      element.clientHeight,
    )
  }

  update(time: TickTime) {
    this.position = this.position.add(this.speed.times(time.delta / 1000));
  }

  draw() {
    this.element.style.left = this.position.x + 'px';
    this.element.style.top  = this.position.y + 'px';
  }

  activate(@Inject(GameLoopService) gameLoopService: GameLoopService) {
    this.gameLoop = gameLoopService;
    gameLoopService.update.subscribe(this.update.bind(this));
    gameLoopService.draw.subscribe(this.draw.bind(this));
  }

  calculateCollision(collider: Entity, cb: (Vector2) => void = null): Vector2 {
    let collision: Vector2 = null;

    const rect1 = {
      x: this.position.x,
      y: this.position.y,
      width: this.dimension.x,
      height: this.dimension.y,
    };
    const rect2 = {
      x: collider.position.x,
      y: collider.position.y,
      width: collider.dimension.x,
      height: collider.dimension.y,
    };

    if (rect1.x < rect2.x + rect2.width &&
      rect1.x + rect1.width > rect2.x &&
      rect1.y < rect2.y + rect2.height &&
      rect1.y + rect1.height > rect2.y) {
      collision = new Vector2(
        collider.position.x + collider.dimension.x / 2,
        collider.position.y + collider.dimension.y / 2,
      )
    }

    if (collision && typeof cb === 'function') {
      cb(collision);
    }

    return collision;
  }


  get element(): HTMLElement {
    return this._element;
  }

  set element(value: HTMLElement) {
    this._element = value;
  }

  get position(): Vector2 {
    return this._position;
  }

  set position(value: Vector2) {
    this._position = value;
  }

  get dimension(): Vector2 {
    return this._dimension;
  }

  set dimension(value: Vector2) {
    this._dimension = value;
  }

  get speed(): Vector2 {
    return this._speed;
  }

  set speed(value: Vector2) {
    this._speed = value;
  }
}
