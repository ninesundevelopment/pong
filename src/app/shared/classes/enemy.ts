import { Entity } from '@app/shared/classes/entity';
import { TickTime } from '@app/core/services/game-loop/game-loop.service';


export class Enemy extends Entity {

  private _target: Entity;

  update(time: TickTime) {
    if (this.target !== null) {
      if (this.position.y + 50 < this.target.position.y) {
        this.speed.y = 200
      }
      if (this.position.y + 50 > this.target.position.y) {
        this.speed.y = -200;
      }
      if(Math.abs(this.position.y + 50 - this.target.position.y) < 50) {
        this.speed.y = 0;
      }
    }

    super.update(time);
  }


  get target(): Entity {
    return this._target;
  }

  set target(value: Entity) {
    this._target = value;
  }
}
