import { Component, ElementRef, HostListener, NgZone, OnInit, ViewChild } from '@angular/core';
import { Vector2 } from '@app/shared/classes/vector2';
import { GameLoopService } from '@app/core/services/game-loop/game-loop.service';
import { Entity } from '@app/shared/classes/entity';
import { Enemy } from '@app/shared/classes/enemy';


@Component({
  selector: 'project',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})
export class GameComponent implements OnInit {

  public fps       = 0;
  public ballspeed = 0;
  public angle     = 0;
  public countdown = 0;

  public gameOver = false;

  public ball: Entity;
  public paddleLeft: Entity;
  public paddleRight: Enemy;

  public ballSpeedMultiplier = 5;
  public ballBaseSpeed       = 500;
  public panelBaseSpeed      = 300;

  public scoreLeft  = 0;
  public scoreRight = 0;

  @ViewChild('ball') refBall: ElementRef;
  @ViewChild('paddleLeft') refPaddleLeft: ElementRef;
  @ViewChild('paddleRight') refPaddleRight: ElementRef;

  constructor(
    private gameLoop: GameLoopService,
    private zone: NgZone,
  ) {
  }

  ngOnInit() {
    this.ball               = new Entity(this.refBall.nativeElement);
    this.paddleLeft         = new Entity(this.refPaddleLeft.nativeElement);
    this.paddleRight        = new Enemy(this.refPaddleRight.nativeElement, new Vector2(window.innerWidth - 20, 0));
    this.paddleRight.target = this.ball;

    this.updateInfo();
    this.resetGame();

    this.ball.activate(this.gameLoop);
    this.paddleLeft.activate(this.gameLoop);
    this.paddleRight.activate(this.gameLoop);

    this.gameLoop.update.subscribe(this.update.bind(this));

    this.scheduleStart(5);
  }

  scheduleStart(timeLeft: number) {
    this.gameLoop.stop();
    this.countdown = timeLeft;

    console.log(timeLeft);

    if (timeLeft === 0) {
      this.resetGame();
      this.gameLoop.start();
    } else {
      window.setTimeout(() => {
        this.scheduleStart(--timeLeft)
      }, 1000);
    }

  }

  updateInfo() {
    this.fps       = Math.ceil(this.gameLoop.getFPS());
    this.ballspeed = Math.ceil(this.ball.speed.magnitude());
    this.angle     = this.ball.speed.angle();

    window.setTimeout(this.updateInfo.bind(this), 100);
  }

  handleGameOver() {
    this.gameLoop.stop();

    this.zone.run(() => {
      this.gameOver = true;
    });
  }

  restartRound() {
    this.zone.run(() => {
      this.ballBaseSpeed += (this.scoreRight + this.scoreLeft) * 5;
    });
    this.resetGame();
  }

  resetGame() {
    this.gameOver = false;

    this.ball.position = new Vector2(window.innerWidth / 2, window.innerHeight / 2);
    this.ball.speed    = Vector2.Left().times(this.ballBaseSpeed * (Math.random() < 0.5 ? -1 : 1));
  }

  update() {
    // Clamp all to boundary
    if (this.ball.position.x + this.ball.dimension.x >= window.innerWidth) {
      this.scoreLeft++;
      this.scheduleStart(3);
    }
    if (this.ball.position.x <= 0) {
      this.scoreRight++;
      this.scheduleStart(3);
    }
    if (this.ball.position.y + this.ball.dimension.y >= window.innerHeight) {
      this.ball.position.y = window.innerHeight - this.ball.dimension.y;
      this.ball.speed.y    = this.ball.speed.y * -1;
    }
    if (this.ball.position.y <= 0) {
      this.ball.position.y = 0;
      this.ball.speed.y    = this.ball.speed.y * -1;
    }


    if (this.paddleLeft.position.y + this.paddleLeft.dimension.y >= window.innerHeight) {
      this.paddleLeft.speed      = Vector2.Zero();
      this.paddleLeft.position.y = window.innerHeight - this.paddleLeft.dimension.y;
    }
    if (this.paddleLeft.position.y <= 0) {
      this.paddleLeft.speed      = Vector2.Zero();
      this.paddleLeft.position.y = 0;
    }

    if (this.paddleRight.position.y + this.paddleRight.dimension.y >= window.innerHeight) {
      this.paddleRight.speed      = Vector2.Zero();
      this.paddleRight.position.y = window.innerHeight - this.paddleRight.dimension.y;
    }
    if (this.paddleRight.position.y <= 0) {
      this.paddleRight.speed      = Vector2.Zero();
      this.paddleRight.position.y = 0;
    }


    this.paddleLeft.calculateCollision(this.ball, (collision) => {
      const verticalSpeedFactor = Math.ceil(Math.abs(collision.y - 50) * 1.25);

      if (collision.y < 50) {
        this.ball.speed = new Vector2(1, -1 * verticalSpeedFactor / 100).unit().times(this.ballBaseSpeed);
      } else {
        this.ball.speed = new Vector2(1, verticalSpeedFactor / 100).unit().times(this.ballBaseSpeed);
      }
      this.ball.position.x = this.paddleLeft.position.x + this.paddleLeft.dimension.x;
    });

    this.paddleRight.calculateCollision(this.ball, (collision) => {
      const verticalSpeedFactor = Math.ceil(Math.abs(collision.y - 50) * 2);

      if (collision.y < 50) {
        this.ball.speed = new Vector2(-1, -1 * verticalSpeedFactor / 100).unit().times(this.ballBaseSpeed);
      } else {
        this.ball.speed = new Vector2(-1, verticalSpeedFactor / 100).unit().times(this.ballBaseSpeed);
      }
      this.ball.position.x = this.paddleRight.position.x - this.ball.dimension.x;
    });
  }

  @HostListener('window:keydown', ['$event'])
  keyDown(event: KeyboardEvent) {
    this.zone.runOutsideAngular(() => {
      console.log(event.key);
      if (event.key === ' ') {
        if (this.gameLoop.isRunning()) {
          this.gameLoop.stop();
        } else {
          this.gameLoop.start();
        }
      }

      if (event.key === 'ArrowDown' && this.paddleLeft.position.y + this.paddleLeft.dimension.y < window.innerHeight) {
        this.paddleLeft.speed = Vector2.Down().times(this.panelBaseSpeed);
      } else if (event.key === 'ArrowUp' && this.paddleLeft.position.y > 0) {
        this.paddleLeft.speed = Vector2.Up().times(this.panelBaseSpeed);
      }
    });
  }

  @HostListener('window:keyup', ['$event'])
  keyUp(event: KeyboardEvent) {
    this.zone.runOutsideAngular(() => {
      if (event.key === 'ArrowDown' && this.paddleLeft.speed.y > 0) {
        this.paddleLeft.speed = Vector2.Zero();
      } else if (event.key === 'ArrowUp' && this.paddleLeft.speed.y < 0) {
        this.paddleLeft.speed = Vector2.Zero();
      }
    });
  }

  restart() {
    this.resetGame();
    this.gameLoop.start();
  }
}
