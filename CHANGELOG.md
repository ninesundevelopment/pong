# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.2](https://bitbucket.org/analysis-first/webapp/compare/v1.1.1...v1.1.2) (2019-03-22)


### Bug Fixes

* adds automated push to release script ([7c3e747](https://bitbucket.org/analysis-first/webapp/commits/7c3e747))



# [1.1.0](https://bitbucket.org/analysis-first/webapp/compare/v1.0.1...v1.1.0) (2019-03-22)


### Features

* Restas and Authentication Service ([2cd870a](https://bitbucket.org/analysis-first/webapp/commits/2cd870a))



## [1.0.1](https://bitbucket.org/analysis-first/webapp/compare/v1.0.0...v1.0.1) (2019-03-21)


### Bug Fixes

* cleans up the code ([8cc3ecd](https://bitbucket.org/analysis-first/webapp/commits/8cc3ecd))



# [1.0.0](https://bitbucket.org/analysis-first/webapp/compare/v0.0.2...v1.0.0) (2019-03-21)


### Features

* New Repository ([ed80644](https://bitbucket.org/analysis-first/webapp/commits/ed80644))


### BREAKING CHANGES

* The old repo is now discontinued.



## [0.0.2](https://bitbucket.org/analysis-first/webapp/compare/v0.0.1...v0.0.2) (2019-03-21)


### Bug Fixes

* removes README.md ([233fac4](https://bitbucket.org/analysis-first/webapp/commits/233fac4))



## 0.0.1 (2019-03-21)
